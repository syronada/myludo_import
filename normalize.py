#
# Permet de normaliser le nom des jeux dans la colonne A d'un fichier excel
# dans la colonne B de chaque onglet
#

import openpyxl
from unidecode import unidecode
import os
import string

# Ouvrir le fichier Excel
wb = openpyxl.load_workbook('f.xlsx')

# Parcourir tous les onglets
for sheet_name in wb.sheetnames:
    sheet = wb[sheet_name]
    
    # Parcourir les valeurs de la colonne A
    for idx, cell in enumerate(sheet['A'], start=1):
        # Vérifier si la cellule n'est pas vide
        if cell.value:
            # Modifier la valeur en retirant les espaces et remplaçant les accents
            new_value = unidecode(str(cell.value).replace(" ", "").lower())
            new_value = new_value.translate(str.maketrans('', '', string.punctuation))

            print(new_value)
            
            # Ajouter la nouvelle valeur dans la colonne B
            sheet.cell(row=idx, column=2).value = new_value

# Enregistrer les modifications dans le fichier Excel
wb.save('f-traite.xlsx')

