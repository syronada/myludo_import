import os
import requests
from dotenv import load_dotenv
import time
import sys


#
# Usage: python import_IDs.py {ID}
# uses tests_files/ID_to_import and lastID.txt
# if ID : restart from this ID  
#   else if file lastID.txt : restart from this ID
#

load_dotenv('.env')


def getData(myID):
    time.sleep(0.5)
    url = 'https://www.myludo.fr/views/game/datas.php?type=game&id=' + str(myID)

    # Les valeurs des en-têtes à envoyer
    csrf_token = os.getenv('X_CSRF_TOKEN')
    cookie = os.getenv('COOKIE')

    # Les en-têtes à envoyer dans la requête POST
    headers = {'X-Csrf-Token': csrf_token, 'Cookie': cookie, 'Accept-Encoding': ''}

    # Envoie de la requête POST
    response = requests.get(url, headers=headers)

    # Affichage du code de réponse HTTP et du contenu de la réponse
    if (response.status_code == 200 ):
        return response
    else:
        print('Code de réponse HTTP:', response.status_code , ' / Contenu de la réponse:', response.text, ' / ', int(myID))
        return -1

def getImg(url):
    time.sleep(0.1)

    # Les valeurs des en-têtes à envoyer
    csrf_token = os.getenv('X_CSRF_TOKEN')
    cookie = os.getenv('COOKIE')

    # Les en-têtes à envoyer dans la requête POST
    headers = {'X-Csrf-Token': csrf_token, 'Cookie': cookie, 'Accept-Encoding': ''}

    # Nom de fichier de l'image
    filename = 'img/' + os.path.basename(url)

    # Vérifier si le fichier existe déjà localement
    if not os.path.exists(filename):
        # Si le fichier n'existe pas, télécharger l'image
        # Envoie de la requête POST
        response = requests.get(url, headers=headers)
        
        # Vérifier si la réponse est valide
        if response.status_code == 200:
            # Enregistrer l'image localement
            with open(filename, 'wb') as f:
                f.write(response.content)
            print('L\'image a été téléchargée et enregistrée sous le nom', filename)
        else:
            print('La requête GET a échoué avec le code de réponse HTTP', response.status_code)
    else:
        print('L\'image existe déjà localement sous le nom', filename)




ok = 0
ko = 0
path = 'report.txt'
f_lastID="lastID.txt"
if os.path.isfile(path):
    os.remove(path)

if len(sys.argv)>1 :
    gogo=False
    startID=int(sys.argv[1])
else :    
    # if os.path.isfile(f_lastID):
    if False :
        gogo=False
        with open(f_lastID, 'r') as file:
            startID = int(file.readline().strip())
    else:
        gogo=True

i=0
with open('tests_files/ID_to_img_get', 'r') as file:
    for line in file:
        i+=1
        line = line.strip() # Enlever les caractères de fin de ligne
        if line.isdigit():
            if gogo :
                r = getData(int(line))
                if ( r.status_code == 200 ):
                    print (f"OK : {line} ({i})")
                    with open(path, 'a') as f:
                        f.write(f"OK,{line}\n")
                    json_data = r.json()

                    if 'image' in json_data and 'S300' in json_data['image']:
                        print('La valeur de image > jpg est:', json_data['image']['S300'])
                        getImg(json_data['image']['S300'])

                    ok += 1
                else:
                    print (f"Déjà en collection ou erreur pour : {line} ({i})")
                    with open(path, 'a') as f:
                        f.write(f"--KO,{line}\n")
                    ko +=1
                with open(f_lastID, 'w') as f:
                    f.write(f"{line}")
            else:
                if int(line)==startID :
                    gogo=True
                print (f"pass : {line}")
                    
        else:
            print(f"Ce n'est pas un nombre : {line}.")

print (f"TERMINE : OK {ok} / KO {ko} ")