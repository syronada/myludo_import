import os
import requests
from dotenv import load_dotenv
import time
import sys


#
# Usage: python import_IDs.py {ID}
# uses tests_files/ID_to_import and lastID.txt
# if ID : restart from this ID  
#   else if file lastID.txt : restart from this ID
#

load_dotenv('.env')


def addToCollection(myID):
    time.sleep(0.5)
    url = 'https://www.myludo.fr/views/game/datas.php'

    # Les valeurs des en-têtes à envoyer
    csrf_token = os.getenv('X_CSRF_TOKEN')
    cookie = os.getenv('COOKIE')

    # Les données à envoyer dans la requête POST
    # payload = {'type': 'remove', 'game': '61611'}
    payload = {'type': 'add', 'game': int(myID)}
    # payload = {'type': 'remove', 'game': int(myID)}

    # url = 'https://www.myludo.fr/views/badges/datas.php'
    # payload = {'type': 'collection', 'game': '61611'}

    # Les en-têtes à envoyer dans la requête POST
    headers = {'X-Csrf-Token': csrf_token, 'Cookie': cookie, 'Accept-Encoding': ''}

    # Envoie de la requête POST
    response = requests.post(url, data=payload, headers=headers)

    # Affichage du code de réponse HTTP et du contenu de la réponse
    if (response.status_code == 200 and response.text == '{"success":true}'):
        return 1
    else:
        print('Code de réponse HTTP:', response.status_code , ' / Contenu de la réponse:', response.text, ' / ', int(myID))
        return -1


ok = 0
ko = 0
path = 'report.txt'
f_lastID="lastID.txt"
if os.path.isfile(path):
    os.remove(path)

if len(sys.argv)>1 :
    gogo=False
    startID=int(sys.argv[1])
else :    
    if os.path.isfile(f_lastID):
        gogo=False
        with open(f_lastID, 'r') as file:
            startID = int(file.readline().strip())
    else:
        gogo=True

i=0
with open('tests_files/ID_to_import', 'r') as file:
    for line in file:
        i+=1
        line = line.strip() # Enlever les caractères de fin de ligne
        if line.isdigit():
            if gogo :
                if ( addToCollection(int(line)) > 0 ):
                    print (f"OK : {line} ({i})")
                    with open(path, 'a') as f:
                        f.write(f"OK,{line}\n")
                    ok += 1
                else:
                    print (f"Déjà en collection ou erreur pour : {line} ({i})")
                    with open(path, 'a') as f:
                        f.write(f"--KO,{line}\n")
                    ko +=1
                with open(f_lastID, 'w') as f:
                    f.write(f"{line}")
            else:
                if int(line)==startID :
                    gogo=True
                print (f"pass : {line}")
                    
        else:
            print(f"Ce n'est pas un nombre : {line}.")

print (f"TERMINE : OK {ok} / KO {ko} ")