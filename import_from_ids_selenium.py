from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
import time
import os
import sys
from myconfig import *
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service as ChromeService
#pip install webdriver-manager


#
#  /!\ This scrit is decaprecied because import_IDs.py
#
# Usage: python import_from_ids_selenim.py {ID}
# uses tests_files/ID_to_import and lastID.txt
# if ID : restart from this ID  
#   else if file lastID.txt : restart from this ID
#

chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
chrome_options.add_argument('--ignore-certificate-errors')
chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])

def login(driver):
    driver.get("https://www.myludo.fr/#!/game/55329")
    
    # wait 2 seconds before looking for element
    element = WebDriverWait(driver, 2).until(
        EC.presence_of_element_located((By.ID, "tarteaucitronPersonalize2"))
    )   
    ActionChains(driver).move_to_element(element).click(element).perform()

    element = driver.find_element(By.CLASS_NAME,"connect")
    element.click()
    
    element = driver.find_element(By.ID,"emaillogin") 
    element.send_keys(myLogin)                      # define in myconfig.py

    element = driver.find_element(By.ID,"password")
    element.send_keys(myPassword)                   # define in myconfig.py

    element = driver.find_element (By.XPATH, "/html/body/header/div[1]/form/div[2]/button")
    ActionChains(driver).move_to_element(element).click(element).perform()


def addGame(driver, id):
    driver.get(f"https://www.myludo.fr/#!/game/{id}")
    # time.sleep(1)
    try:
        element = driver.find_element (By.XPATH,"//a[@title='Ajouter à ma collection']")
        ActionChains(driver).move_to_element(element).click(element).perform()
    except NoSuchElementException:
        # print(f"Erreur pour : {id}") # deja en liste
        return (-1)
        pass
    return (id)

def supprGame(driver, id):
    driver.get(f"https://www.myludo.fr/#!/game/{id}")
    # time.sleep(1)
    try:
        element = driver.find_element (By.XPATH,"//a[@title='Supprimer de ma collection']")
    except NoSuchElementException:
        return (-1)
        pass
    # ActionChains(driver).move_to_element(element).click(element).perform()
    element.click()
    try:
        element = driver.find_element (By.XPATH,"/html/body/header/div[9]/div[2]/a[2]")
    except NoSuchElementException:
        return (-1)
        pass
    # ActionChains(driver).move_to_element(element).click(element).perform()
    element.click()
    return (id)



# driver = webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install()))
service = ChromeService(executable_path=ChromeDriverManager().install())
driver = webdriver.Chrome(service=service)

driver.implicitly_wait(1.5)

login(driver)

ok = 0
ko = 0
path = 'report.txt'
f_lastID="lastID.txt"
if os.path.isfile(path):
    os.remove(path)

if len(sys.argv)>1 :
    gogo=False
    startID=int(sys.argv[1])
else :    
    if os.path.isfile(f_lastID):
        gogo=False
        with open(f_lastID, 'r') as file:
            startID = int(file.readline().strip())
    else:
        gogo=True
i=0
with open('tests_files/ID_to_import', 'r') as file:
    for line in file:
        i+=1
        line = line.strip() # Enlever les caractères de fin de ligne
        if line.isdigit():
            if gogo :
                if addGame(driver,int(line)) > 0 :
                # if supprGame(driver,int(line)) > 0 :
                    print (f"OK : {line} ({i})")
                    with open(path, 'a') as f:
                        f.write(f"OK,{line}\n")
                    ok += 1
                else:
                    print (f"Déjà en collection ou erreur pour : {line} ({i})")
                    with open(path, 'a') as f:
                        f.write(f"--KO,{line}\n")
                    ko +=1
                with open(f_lastID, 'w') as f:
                    f.write(f"{line}")
            else:
                if int(line)==startID :
                    gogo=True
                print (f"pass : {line}")
                    
        else:
            print(f"Ce n'est pas un nombre : {line}.")

print (f"TERMINE : OK {ok} / KO {ko} ")
